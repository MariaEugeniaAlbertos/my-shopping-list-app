import "../styles/MainList.scss";
import Card from "./Card";

function MainList({ itemsToComplete, setItemsToComplete }) {
  const onChangeStatus = (e) => {
    const { name, checked } = e.target;

    const updateList = itemsToComplete.map((item) => ({
      ...item,
       done: item.id === name ? checked : item.done,
    }));
    setItemsToComplete(updateList);
  };

  const onClickRemoveItem = (e) => {
    const updateList = itemsToComplete.filter((item) => !item.done);
    setItemsToComplete(updateList);
  };

  const card = itemsToComplete.map((item) => (
    <Card key={item.index} data={item} onChange={onChangeStatus} />
  ));

  return (
    <div className="items__list">
      
      {card}

      <button onClick={onClickRemoveItem}>DELETE ALL</button>
    </div>
  );
}

export default MainList;
