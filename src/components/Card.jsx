import "../styles/Card.scss";



function Card ({ onChange, data: { id, newItem, done }}) {

  return (
    <div>
        <input
          className="todo__state"
          name={id}
          type="checkbox"
          defaultChecked={done}
          onChange={onChange}
        />
        <div className="todo__text">{newItem}</div>
      
    </div>
  );
};

export default Card;