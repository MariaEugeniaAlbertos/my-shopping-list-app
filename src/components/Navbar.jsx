import "../styles/Navbar.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
 
  faList,
  faShoppingCart,

} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <div className="navbar">
      <div className="navbar__left-control">
        <span className="icon-container">
          <Link to="/">
            <FontAwesomeIcon icon={faShoppingCart} className="shopping-icon" />
          </Link>
        </span>
        <div className="find-container">
          <input placeholder="Búsqueda" autoComplete="off"></input>
        </div>
      </div>
      <div className="navbar__right-control">
        <span className="icon-container">
          <Link to="/list">
            <FontAwesomeIcon icon={faList} className="list-icon" />{" "}
          </Link>
        </span>
      </div>
    </div>
  );
}

export default Navbar;
