import "../styles/FormTodo.scss";
import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

function FormTodo({ addItem }) {
  const [newItem, setNewItem] = useState("");
  const [showForm, setShowForm] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log("Hemos hecho submit!");
    addItem({
      done: false,
      id: (+new Date()).toString(),
      newItem,
    });
    setNewItem("");
  };

  const handleChange = (event) => {
    const newValue = event.target.value;
    console.log(newValue);
    setNewItem(newValue);
  };

  return (
    <section>
      <h2>Add a product in your shopping list!</h2>
      <button onClick={() => setShowForm(!showForm)}>
        <FontAwesomeIcon icon={faPlus} className="plus-icon" />
      </button>
      {showForm ? (
        <form onSubmit={handleSubmit}>
          <input type="text" value={newItem} onChange={handleChange}></input>
          <button type="submit" disabled={newItem ? "" : "disabled"}>
            ADD PRODUCT
          </button>
        </form>
      ) : null}
    </section>
  );
}

export default FormTodo;
