import "./App.scss";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import FormTodo from "./components/FormTodo";
import MainList from "./components/MainList";

import { useState } from "react";

function App() {
  const [itemsToComplete, setItemsToComplete] = useState([]);
  function addItem(newItem) {
    setItemsToComplete([
      ...itemsToComplete, newItem
    ])
  }
  return (
    <Router>
      <div className="App">
        <Navbar/>
        <Switch>
          <Route path="/" exact>
            <FormTodo addItem={addItem}/>
          </Route>
          <Route path="/list" exact>
            <MainList itemsToComplete={itemsToComplete} setItemsToComplete={setItemsToComplete}/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
